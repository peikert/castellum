# Generated by Django 4.2.11 on 2024-04-03 08:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("subjects", "0055_alter_consent_id_alter_consentdocument_id_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="consent",
            name="full_age_notified_1",
            field=models.BooleanField(
                default=False, verbose_name="First expiration notification sent"
            ),
        ),
        migrations.AddField(
            model_name="consent",
            name="full_age_notified_2",
            field=models.BooleanField(
                default=False, verbose_name="Second expiration notification sent"
            ),
        ),
    ]
