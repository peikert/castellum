# Generated by Django 4.2.11 on 2024-03-25 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("pseudonyms", "0008_domain_managers"),
    ]

    operations = [
        migrations.AlterField(
            model_name="domain",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
        migrations.AlterField(
            model_name="pseudonym",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
    ]
