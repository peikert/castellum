You can interact directly with [django
models](https://docs.djangoproject.com/en/stable/ref/models/querysets/)
to perform tasks that cannot be covered by Castellum's general-purpose
UI. This folder contains some examples for common tasks. Feel free to
submit your own script if you feel it could be helpful to other.

To use these scripts, customize them for your specific use case and copy
them to ``castellum/subjects/management/commands/``. You can then
execute them using ``python3 -m django``.
